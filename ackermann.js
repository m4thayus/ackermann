function ack(m, n) {
    if (m === 0) {
        return n + 1;
    } else if (n === 0) {
        return ack(m - 1, 1);
    } else {
        return ack(m - 1, ack(m, n - 1));
    }
}

for (i in Array(5)) {
    for (j in Array(5)) {
        console.log(`ack(${i}, ${j}) = ${ack(i, j)}`);
    }
}