def ack(m, n)
    case 0
    when m
        n + 1 
    when n
        ack(m - 1, 1)
    else
        ack(m - 1, ack(m, n - 1))
    end
end

for i in 0..5 do
    for j in 0..5 do
        puts "ack(#{i}, #{j}) = #{ack(i, j)}"
    end
end

