fn main() {
    for i in 0..5 {
        for j in 0..5 {
            println!("ack({}, {}) = {}", i, j, ack(i,j));
        }
    }
}

fn ack(m: u32, n:u32) -> u32 {
    let ans: u32 =
        if m == 0 {
            n + 1
        } else if n == 0 {
            ack(m - 1, 1)
        } else {
            ack(m - 1, ack(m, n - 1))
    };
    return ans;
}